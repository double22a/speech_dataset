## paper list

- 2006, largest open-source Russian language dataset, [Exploration of End-to-End ASR for OpenSTT–Russian Open Speech-to-Text Dataset](https://arxiv.org/pdf/2006.08274.pdf)

- 2012, LARGE-SCALE MULTILINGUAL DATASET, [MLS: A LARGE-SCALE MULTILINGUAL DATASET FOR SPEECH RESEARCH](https://arxiv.org/abs/2012.03411)

- 2101, Multilingual Speech Corpus, [VoxPopuli: A Large-Scale Multilingual Speech Corpus for Representation Learning, Semi-Supervised Learning and Interpretation](https://arxiv.org/pdf/2101.00390.pdf)

- 2104, professionally transcribed earnings calls, [SPGISpeech: 5,000 hours of transcribed financial audio for fully formatted end-to-end speech recognition](https://arxiv.org/abs/2104.02014)

- 2106, multi-domain English speech recognition corpus, [GigaSpeech: An Evolving, Multi-domain ASR Corpus with 10,000 Hours of Transcribed Audio](https://arxiv.org/abs/2106.06909)

- 2107, Large-Scale Diverse English Dataset, [The People’s Speech: A Large-Scale Diverse English Speech Recognition Dataset for Commercial Usage](https://openreview.net/pdf?id=R8CwidgJ0yT)
